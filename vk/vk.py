import os, inspect
from grab import Grab
from bs4 import BeautifulSoup

class VK():
    def __init__(self):
        self.logging = True
        self.credentials = self.loadLogins()
        self.groups = self.loadGroups()
        self.gr = Grab()

    def loadLogins(self, path='Logins.txt'):
        path = self.preParePath(path)
        logins = {}
        try:
            file = open(path, "r")
        except IOError as e:
            self.log(e)
        else:
            for line in file:
                data = line.split(":")
                logins[data[0]] = data[1]
            file.close()
            return logins

    def preParePath(self, path):
        filename = inspect.getframeinfo(inspect.currentframe()).filename
        path_ = os.path.dirname(os.path.abspath(filename))
        fileDir = os.path.dirname(path_)
        return os.path.abspath(os.path.realpath(os.path.join(fileDir, path)))

    def log(self, msg):
        if self.logging:
            print(msg)

    def loadGroups(self, path='groups.txt'):
        path = self.preParePath(path)
        groups = []
        try:
            file = open(path, "r")
        except IOError as e:
            self.log(e)
        else:
            for line in file:
                groups.append(line)
            file.close()
            return groups

    def checkIfLoggedIn(self):
        self.gr.go('https://vk.com')
        try:
            self.gr.css_text('#top_profile_link > div.top_profile_name')
            return True
        except IndexError:
            return False

    def loginUser(self, userName, password):
        self.gr.go("https://vk.com")
        self.gr.set_input('email', userName)
        self.gr.set_input('pass', password)
        self.gr.submit()

    def joinGroups(self):
        for group in self.groups:
            self.gr.go(group)
            soup = BeautifulSoup(self.gr.response.body, 'html.parser')
            joinElement = soup.find(class_='button wide_button')
            self.gr.go('https://m.vk.com' + joinElement.get('href'))

    def clean(self):
        self.gr.clear_cookies()

